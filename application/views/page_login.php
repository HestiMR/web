<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css');?>">
<link rel="stylesheet" href="<?php echo base_url()."assets/";?>bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="<?php echo base_url()."assets/";?>dist/css/AdminLTE.min.css">
<style>
	.loginbox{
		margin:180px auto;
		width: 450px;
		position: relative;
		border-radius: 15px;
		background: #ffffff;
	}
	body{
		background-color: rgb(209,209,209);
	}
</style>


	<title>Halaman Login</title>
</head>
<body>
<div class="box box-info loginbox">
            <div class="box-header with-border">
              <h3 class="box-title">Form Login</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->



            <form class="form-horizontal" method="POST">
              <div class="box-body">
                <div class="form-group">
                  <label for="inputUsername" class="col-sm-2 control-label">Username</label>

                  <div class="col-sm-10">
                    <input type="email" class="form-control" id="inputUsername" name="user" placeholder="Username">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputPassword" class="col-sm-2 control-label">Password</label>

                  <div class="col-sm-10">
                    <input type="password" class="form-control" name="pass" id="inputPassword" placeholder="Password">
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-offset-2 col-sm-10">
                    <div class="checkbox">
                      <label>
                        <input type="checkbox"> Remember me
                      </label>
                    </div>
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-default">Cancel</button>
                <!-- <button type="submit" name="masuk" class="btn btn-info pull-right">Sign in</button> -->
                <a href="page_admin" class="btn btn-info pull-right">Sign in</a>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>

          <script src="<?php echo base_url('assets/bootstrap/js/bootstrap.min.js');?>"></script>

	 <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script>
</body>
</html>